<?php
/*
Plugin Name:  Wordpress Enhancements
Description:  Useful hooks a filters for Wordpress.
Version:      1.0.0
Author:       Marlow Perceval
License:      MIT License
*/

add_filter('get_post_metadata', function ($value, $post_id, $meta_key, $single) {
	// We want to pass the actual _thumbnail_id into the filter, so requires recursion
	static $is_recursing = false;

	// Only filter if we're not recursing and if it is a post thumbnail ID
	if (! $is_recursing && $meta_key === '_thumbnail_id') {
		$is_recursing = true; // prevent this conditional when get_post_thumbnail_id() is called
		$value = get_post_thumbnail_id($post_id);
		$is_recursing = false;
		$value = apply_filters('post_thumbnail_id', $value, $post_id);
		if (! $single) {
			$value = array($value);
		}
	}
	return $value;
}, 10, 4);

global $shortcodes_to_remove_from_content;
$shortcodes_to_remove_from_content = array();

function get_shortcode($name = '', $conditions = array(), $remove_from_content = true, $do_shortcode = true, $instance = false) {
	if (empty($name)) return '';

	global $shortcodes_to_remove_from_content;
	global $post;

	// $prop_to_match = array($name);
	$prop_to_match = array();
	foreach ($conditions as $attr => $regex) {
		$prop_to_match[] = $attr . '="' . $regex . '"';
	}

	$output = false;

	if (preg_match_all('/' . get_shortcode_regex() . '/s', $post->post_content, $matches, PREG_SET_ORDER)) {
		if (!empty($matches[0])) {
			$shortcodes = $matches[0];
			$name_instance = 0;

			foreach ($matches as $key => $match) {
				$shortcode = $match[0];

				if (strpos($shortcode, '[' . $name) !== false) {
					$name_instance++;
					if ($instance !== false && $name_instance !== $instance) {
						continue;
					}
					$match_count = 0;
					foreach ($prop_to_match as $prop_regex) {
						if (preg_match('/' . $prop_regex . '/', $shortcode)) {
							$match_count++;
						} else {
							continue;
						}
					}
					if ($match_count == count($prop_to_match)) {
						$output = $shortcode;
						if ($remove_from_content) {
							$shortcodes_to_remove_from_content[] = $shortcode;
						}
						break;
					}
				}
			}
		}
	}
	if ($output && $do_shortcode) {
		$output = do_shortcode($output);
	}
	return $output;
}

function remove_shortcodes($content) {
	global $shortcodes_to_remove_from_content;
	foreach ($shortcodes_to_remove_from_content as $key => $shortcode) {
		$content = str_replace($shortcode, '', $content);
	}
	return $content;
}
add_filter( 'the_content',  __NAMESPACE__ . '\\remove_shortcodes' );

function is_ajax() {
	return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
}